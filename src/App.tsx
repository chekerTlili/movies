import * as React from "react";
import {  VStack} from "@chakra-ui/react";
import MoviesList from "./components/MoviesList";
import { movie } from "./Model";
import SearchForm from "./components/formSearch/SearchForm";
import NavBar from "./components/NavBar";


export const App = () => {

  const [movies, setMovies] = React.useState<movie[]>([]);

  const getByGenreRequest = () =>{
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': '6ac22d8427mshf99ef838b565c1fp102d3ajsn0ac3895436aa',
        'X-RapidAPI-Host': 'moviesdatabase.p.rapidapi.com'
      }
    };
    
    fetch('https://moviesdatabase.p.rapidapi.com/titles?genre=Drama', options)
      .then(response => response.json())
      .then(response => setMovies(response.results))
      .catch(err => console.error(err));
  }

 React.useEffect(()=>{
  getByGenreRequest();
  },[])


return(
  <>
    
  
    <NavBar/>
    <VStack p={4}>
    <SearchForm />
    <MoviesList movies={movies} />
  </VStack>
  </>
  )
}
