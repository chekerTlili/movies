import { Box, Button, FormControl, HStack, Input, Select } from '@chakra-ui/react';
import React, { useState } from 'react'
import GenresList from './GenresList';




function SearchForm() {
  const [movieTitle, setMovieTitle] = useState <string>('');
  const [movieGenre, setMovieGenre] = useState <string>('');

   

  const handelSubmit =()=>{
      console.log(movieTitle);
      console.log(movieGenre);
  }
  return (

  <form onSubmit={handelSubmit}>
    <Box mb={'4'} mt='10' ml={'50'}>
    <HStack spacing={8}>
    <Input type='search' 
      variant='filled' 
      w="100" 
      placeholder='Movie title' 
      value={movieTitle} 
      onChange={
                  (e)=>setMovieTitle(e.target.value)
                }
/>
    <GenresList movieGenre={movieGenre} setMovieGenre={setMovieGenre}/>
    <Button variant='solid' type='submit'>Search</Button>
    </HStack>
    </Box>
  </form>
  )
}

export default SearchForm;