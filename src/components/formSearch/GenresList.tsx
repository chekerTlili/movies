import { Select } from '@chakra-ui/react';
import *as React from 'react'


interface Props{
  movieGenre :string;
  setMovieGenre :React.Dispatch<React.SetStateAction<string>>;
}

function GenresList({movieGenre, setMovieGenre}:Props) {

  const [listGenre,setListGenre]=React.useState<string[]>([]);

  const getGenresRequest = () =>{
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': '6ac22d8427mshf99ef838b565c1fp102d3ajsn0ac3895436aa',
        'X-RapidAPI-Host': 'moviesdatabase.p.rapidapi.com'
      }
    };
    
    fetch('https://moviesdatabase.p.rapidapi.com/titles/utils/genres', options)
      .then(response => response.json())
      .then(response => setListGenre(response.results))
      .catch(err => console.error(err));
  }
  
  React.useEffect(()=>{
    getGenresRequest();
  },[])

  return (
<Select placeholder='Select a movie genre' 
      w="100" 
      value={movieGenre}
      onChange={
        (e)=>setMovieGenre(e.target.value)
          }
      >        
        { listGenre.map((movieGenre:string ,index:number)=>
          <option value={movieGenre} key={index}>{movieGenre}</option>)}
    </Select>
  )
}

export default GenresList