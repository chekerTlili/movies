import React, {useEffect,useState} from 'react';
import {FormControl, FormErrorMessage , Input, InputProps} from '@chakra-ui/react';
import {FieldProps ,useField} from '@formiz/core';

export type FieldInputProps = FieldProps 
& Pick <InputProps, 'type' | 'placeholder'> 
& {size?: 'sm' | 'md'| 'lg';
autofocus?: boolean};


function InputField( props:FieldInputProps) {

    const{ errorMessage,
      id,
      isValid,
      isPristine,
      isSubmitted,
      resetKey,
      setValue,
      value,
      } = useField(props);
  
   
  
      const [isTouched, setIsTouched] = useState(false);
      const showError = !isValid && ((isTouched && !isPristine) || isSubmitted);
      const handelChange=(e: { target: { value: string; }; }) =>{ 
        setValue(e.target.value);
         }
  
  useEffect(() => {
  setIsTouched(false);
  }, [resetKey]);


  return (
<FormControl id={id} isRequired={true} isInvalid={showError}>
  
  <Input
    key={resetKey}
    id={id}
    value={value || ''}
    onChange={handelChange}
    onFocus={() => setIsTouched(true)}
    onBlur={() => setIsTouched(false)}
  />
  
  <FormErrorMessage>{errorMessage}</FormErrorMessage>

  
</FormControl>

  )
}

export default InputField