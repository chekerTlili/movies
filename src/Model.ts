export interface movie{
    Poster: string | undefined;
    title:string;
    releaseDate:string;
    description: string;
}